﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LVL2_ASPNet_MVC_04.Models;

namespace LVL2_ASPNet_MVC_04.Controllers
{
    public class HomeController : Controller
    {
        Db_Customer_2Entities dbmodel = new Db_Customer_2Entities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About(tbl_User user)
        {
            var users = dbmodel.tbl_User.Where(a => a.Id == 1).FirstOrDefault();

            return Json(users, JsonRequestBehavior.AllowGet);

        }

 

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}